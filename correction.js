function reverseString(input){
    var strToTab = input.split("");
    var tabReverse = strToTab.reverse();
    var tabToStr = tabReverse.join("");

    return tabToStr ;
}

// console.log(reverseString("tsanta"));

function fizzBuzz(nombre) {
    if ((nombre % 3 === 0) && (nombre % 5 === 0)) {
        return "fizzBuzz";
    }
    if ((nombre % 3) === 0) {
        return "fizz";
    }
    if ((nombre % 5)  === 0) {
        return "Buzz";
    }

    return nombre;
}

// console.log(fizzBuzz(25));

function uniqueTab(tab) {
    var newTab = [];
    for (i = 0; i < tab.length; i++) {
        if(newTab.indexOf(tab[i]) === -1) {
            newTab.push(tab[i]);
        }
    }
    return newTab;
}

// console.log(uniqueTab([25, 2, 2, 4, 4, 4]));

function stringPalindrone(text) {
    var tableauText = text.split("");
    var reversetableauText = tableauText.reverse();
    var tabToStr = reversetableauText.join("")

    return text === tabToStr;
}

console.log(stringPalindrone("tsanta")); 
 
function reverseSentense(text) {
    var strToTab = text.split(" ");
    var tabReverse = strToTab.reverse();
    var tabToStr = tabReverse.join(" ");

    return tabToStr ;
}

console.log(reverseSentense("Tsanta kely ohh"));